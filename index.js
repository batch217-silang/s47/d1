// console.log("Hello World");
// querySelector - 

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");



// we want the fields to appear as well on the Full Name span
const updateFullName = () => {
// we need a variable to store datas from the document then to get the value, use dot notation.. 

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	// innerHTML is empty section between the span with the use of templete literal using the backticks ``

	spanFullName.innerHTML = `${firstName} ${lastName}`;
}
// JS has different events 
//  link: https://www.w3schools.com/jsref/dom_obj_event.asp
// argument "keyup" then the listened info will be placed in updateFullName 

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);
